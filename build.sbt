scalaVersion := "2.10.4"

resolvers += "Scalaz Bintray Repo" at "http://dl.bintray.com/scalaz/releases"

scalacOptions := Seq("-deprecation", "-unchecked", "-feature", "-language:_", "-Xfatal-warnings", "-Xlog-free-terms", "-target:jvm-1.6", "-Xlint", "-Yno-adapted-args", "-Ywarn-all", "-Ywarn-dead-code", "-Ywarn-numeric-widen", "-Ywarn-value-discard")

libraryDependencies ++= Seq(
      "org.scalaz"          %% "scalaz-core"       % "7.0.6",
      "org.scalaz"          %% "scalaz-effect"     % "7.0.6",
      "org.scalaz"          %% "scalaz-concurrent" % "7.0.6",
      "org.scalaz.stream"   %% "scalaz-stream"     % "0.4.1",
      "io.kadai"            %% "kadai"             % "1.5.1",
      "com.amazonaws"       %  "aws-java-sdk"      % "1.7.1",
      "com.github.scopt"    %% "scopt"             % "3.2.0",
      "org.specs2"          %% "specs2"            % "2.3.11"  % "test",
      "org.scalacheck"      %% "scalacheck"        % "1.11.3"  % "test"
    )

version in ThisBuild := "0.1.0-SNAPSHOT"
