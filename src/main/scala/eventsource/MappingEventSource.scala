package eventsource

import com.amazonaws.services.dynamodbv2.{ AmazonDynamoDBClient => DynamoClient }
import com.amazonaws.services.dynamodbv2.model.ConditionalCheckFailedException

import Transform.{ Delete, Insert }
import eventsource.aws.dynamodb.DynamoDB.OverwriteMode
import eventsource.aws.dynamodb._
import scalaz.{ Catchable, Monad, \/, \/-, -\/ }
import scalaz.concurrent.Task
import scalaz.stream.Process
import scalaz.syntax.id.ToIdOps

object MappingEventSource extends EventSource[LogicalKey, ContentKey] {

  class DynamoCommits(
    client: DynamoClient)(
      implicit val M: Monad[Task],
      val C: Catchable[Task]) extends Storage[Task] {

    import scalaz.syntax.monad._
    import DynamoMappings._

    /**
     * To return a stream of commits from Dynamo, we first need to execute a query, then emit results, and then optionally
     * recursively execute the next query until there is nothing more to query.
     *
     * @param key The key
     * @return Stream of commits.
     */
    override def get(key: LogicalKey): Process[Task, Commit] = {
      val query = Query.forHash(key)

      import Process._

      def requestPage(q: Query[Commit]): Task[Page[Commit]] = Task.suspend {
        DynamoDB.query(q).run(client)
      }

      /**
       * Loop generates a stream of commits by returning an Await process (from await), generated with a 'req' of a Task
       * that executes the query, and a 'recv' function that emits the results, and when thats done generates the next
       * Await state.
       */
      def loop(pt: Task[Page[Commit]]): Process[Task, Commit] =
        await(pt) { page =>
          emitAll(page.result) ++ {
            page.next match {
              case None            => halt
              case Some(nextQuery) => loop(requestPage(nextQuery))
            }
          }
        }

      loop(requestPage(query))
    }

    /**
     * To save a commit, we need to enable [[OverwriteMode.NoOverwrite]] and also catch [[ConditionalCheckFailedException]],
     * which represents a duplicate commit.
     * @param commit The commit to save.
     * @return Either an EventSourceError or the commit that was saved. Other non-specific errors should be available
     *         through the container F.
     */
    override def put(commit: Commit): Task[\/[EventSourceError, Commit]] =
      for {
        putResult <-
          DynamoDB.put[CommitId, Commit](commit.id, commit, OverwriteMode.NoOverwrite)
            .map { _ => commit }
            .run(client).attempt
        r <- putResult match {
          case -\/((d: ConditionalCheckFailedException)) =>
            val error: EventSourceError = EventSourceError.DuplicateCommit
            error.left[Commit].point[Task]
          case -\/(t) =>
            C.fail(t)
          case \/-(c) =>
            c.right.point[Task]
        }
      } yield r
  }


  //-----------------------------------------------------------------------------------
  // Below here are just mappings so that we can marshall/unmarshall things in Dynamo.
  //-----------------------------------------------------------------------------------
  import DynamoMappings._

  implicit val tableMapping =
    TableDefinition.from[CommitId, Commit]("sshek-demo-mapping-commits", Mappings.logicalKey.name,
      Some(AttributeDefinition.number(Mappings.sequence.name)))

  implicit val CommitIdKeyEncoder =
    Encoder.StringEncode.contramap[CommitId] {
      case CommitId(key, _) => key.key
    }

  implicit val CommitIdKeyMarshaller =
    Marshaller.fromColumn2[LogicalKey, Sequence, CommitId](Mappings.logicalKey, Mappings.sequence) {
      case CommitId(key, sequence) => (key, sequence)
    }

  implicit val CommitMarshaller =
    Marshaller.from[Commit] {
      case Commit(CommitId(logicalKey, sequence), time, op) =>
        Map(
          Mappings.lastModified(time)
        ) ++ {
          op match {
            case Insert(v) =>
              Marshaller[ContentKey].toMap(v) ++
                Marshaller.fromColumn(Mappings.operation).toMap(TransformOp.Insert)
            case Delete() =>
              Marshaller.fromColumn(Mappings.operation).toMap(TransformOp.Delete)
          }
        }
    }

  implicit val CommitUnmarshaller =
    Unmarshaller.from[Commit](
      for {
        op <- Mappings.operation.get
        logicalKey <- Mappings.logicalKey.get
        sequence <- Mappings.sequence.get
        lastModified <- Mappings.lastModified.get
        op2 <- op match {
          case TransformOp.Insert =>
            Unmarshaller[ContentKey].unmarshall.flatMap { blobMapping =>
              Unmarshaller.Operation.ok[Transform[ContentKey]](Insert(blobMapping))
            }
          case TransformOp.Delete =>
            Unmarshaller.Operation.ok[Transform[ContentKey]](Delete())
        }
      } yield Commit(CommitId(logicalKey, sequence), lastModified, op2)
    )

  implicit val LogicalKeyColumn = Mappings.logicalKey

  implicit val CommitDynamoValue: StoreValue[Commit] = StoreValue.withUpdated {
    (o, a) => StoreValue.newFromValues(a)
  }

  class MappingAPI(val store: Storage[Task])(implicit val M: Monad[Task], val C: Catchable[Task]) extends API[Task] {
    override def createTransform(old: Option[ContentKey], newValue: Option[ContentKey]): Transform[ContentKey] =
      newValue match {
        case None    => Delete()
        case Some(v) => Insert(v)
      }
  }
}

