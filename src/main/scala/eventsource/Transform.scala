package eventsource

import scalaz.std.option._
import scalaz.syntax.std.option._

/**
 * Transform essentially wraps a function from Option[A] => Option[A], but adds a little structure so that it can be
 * stored easily in a commit object.
 * @tparam A
 */
sealed trait Transform[A] {
  import Transform._
  def value: Option[A] =
    this match {
      case Insert(a) => a.some
      case Delete()    => none
    }

  def apply(old: Option[A]): Option[A] = value
}

object Transform {
  def delete[A]: Transform[A] = Delete()
  case class Insert[A](a: A) extends Transform[A]
  case class Delete[A]() extends Transform[A]
}

sealed trait TransformOp
object TransformOp {
  case object Insert extends TransformOp
  case object Delete extends TransformOp

  def apply(s: String): Option[TransformOp] =
    s.toLowerCase match {
      case "insert" => Some(Insert)
      case "delete" => Some(Delete)
      case _        => None
    }

  def apply(op: TransformOp): String =
    op match {
      case Insert => "insert"
      case Delete => "delete"
    }
}
