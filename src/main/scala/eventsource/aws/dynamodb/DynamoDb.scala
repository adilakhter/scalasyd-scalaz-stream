package eventsource.aws.dynamodb

import scalaz.syntax.id._
import scalaz.std.option._
import scalaz.concurrent.Task
import com.amazonaws.services.dynamodbv2.model._
import scala.collection.JavaConverters._
import scalaz.syntax.traverse._
import scalaz.std.list._

/**
 * Contains functions that perform operations on a DynamoDB table. Functions return a DynamoDBAction that can be run by
 * providing an instance of an AmazonDynamoDBClient.
 *
 * Tables are represented as key-value mappings, so you need classes to represent the key and the value. In addition,
 * you need to create instances of:
 *   * [[TableDefinition]] for the table.
 *   * [[Marshaller]] for the key
 *   * [[Marshaller]], [[Unmarshaller]] and [[StoreValue]] for the value
 */
object DynamoDB {

  def query[A](query: Query[A])(implicit evValueUnmarshaller: Unmarshaller[A]): DynamoDBAction[Page[A]] =
    DynamoDBAction.withClient { client =>
      client.query(query.asQueryRequest)
    } flatMap { result =>
      result.getItems.asScala.toList.traverse[DynamoDBAction, A] { m =>
        unmarshall(m)
      } map { values =>
        val lastKey = Option(result.getLastEvaluatedKey)
        val next = lastKey.map { lk =>
          Query.nextFromQuery(query, lk.asScala.toMap)
        }
        Page(values, next)
      }
    }

  def put[A, B](key: A, value: B, overwrite: OverwriteMode = OverwriteMode.Overwrite)(
    implicit evKeyMarshaller: Marshaller[A],
    evValueMarshaller: Marshaller[B],
    evValueUnmarshaller: Unmarshaller[B],
    evValue: StoreValue[B],
    evMapping: TableDefinition[A, B]): DynamoDBAction[Option[B]] =
    doUpdate(key, evValue.asNew(value), overwrite)

  sealed trait ReadConsistency
  object ReadConsistency {
    case object Strong extends ReadConsistency
    case object Eventual extends ReadConsistency

    private[dynamodb] val asBool: ReadConsistency => Boolean = {
      case Strong   => true
      case Eventual => false
    }
  }

  sealed trait OverwriteMode
  object OverwriteMode {
    case object Overwrite extends OverwriteMode
    case object NoOverwrite extends OverwriteMode
  }

  private def doUpdate[A, B](key: A, updateItemRequestEndo: UpdateItemRequestEndo, overwrite: OverwriteMode)(
    implicit evKeyMarshaller: Marshaller[A],
    evValueMarshaller: Marshaller[B],
    evValueUnmarshaller: Unmarshaller[B],
    evValue: StoreValue[B],
    evMapping: TableDefinition[A, B]): DynamoDBAction[Option[B]] =
    DynamoDBAction.withClient {
      _.updateItem {
        new UpdateItemRequest()
          .withTableName(evMapping.name)
          .withReturnValues(ReturnValue.ALL_OLD)
          .withKey(evKeyMarshaller.toFlattenedMap(key).asJava) |> updateItemRequestEndo.run |> {
            req =>
              overwrite match {
                case OverwriteMode.NoOverwrite =>
                  req.withExpected {
                    evKeyMarshaller.toFlattenedMap(key).map {
                      case (col, _) =>
                        col -> new ExpectedAttributeValue().withExists(false)
                    }.asJava
                  }
                case _ =>
                  req
              }
          }
      }
    }.flatMap { result => unmarshallOpt(result.getAttributes) }
}
