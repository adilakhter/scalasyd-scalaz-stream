package eventsource.aws.dynamodb

import com.amazonaws.services.dynamodbv2.model.AttributeValue
import org.joda.time.{DateTimeZone, DateTime}

import scalaz.Contravariant
import scalaz.syntax.std.option._
import scalaz.syntax.id._

case class Encoder[A](run: A => Option[AttributeValue]) {
  def apply(a: A): Option[AttributeValue] =
    run(a)

  def unapply(a: A): Option[AttributeValue] =
    this(a)

  def contramap[B](f: B => A) =
    Encoder(f andThen run)
}

object Encoder {
  def apply[A: Encoder] =
    implicitly[Encoder[A]]

  private def attribute[A](f: A => AttributeValue => AttributeValue): Encoder[A] =
    Encoder { a => (new AttributeValue() <| { f(a) }).some }

  implicit def LongEncode: Encoder[Long] =
    attribute { l => _.withN(l.toString) }

  implicit def IntEncode: Encoder[Int] =
    attribute { i => _.withN(i.toString) }

  implicit def StringEncode: Encoder[String] =
    Encoder { s =>
      // Encode an empty string as no attribute value (DynamoDB doesn't support empty string for attribute value)
      if (s.isEmpty) None
      else new AttributeValue().withS(s).some
    }

  implicit def DateTimeEncode: Encoder[DateTime] =
    attribute { d => _.withN(d.withZone(DateTimeZone.UTC).toInstant.getMillis.toString) }

  implicit def OptionEncode[A](implicit e: Encoder[A]): Encoder[Option[A]] =
    Encoder { _.flatMap { e.run } }

  implicit object EncoderCovariant extends Contravariant[Encoder] {
    def contramap[A, B](r: Encoder[A])(f: B => A) =
      r contramap f
  }
}
