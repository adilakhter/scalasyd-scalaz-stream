package eventsource.aws.dynamodb

import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient
import kadai.Attempt

import scalaz.Monad
import scalaz.concurrent.Task

case class DynamoDBAction[A](unsafeRun: AmazonDynamoDBClient => Task[A]) {
  def map[B](f: A => B): DynamoDBAction[B] =
    flatMap[B](f andThen DynamoDBAction.ok)

  def flatMap[B](f: A => DynamoDBAction[B]): DynamoDBAction[B] =
    DynamoDBAction(client =>
      run(client).flatMap { a =>
        f(a).run(client)
      }
    ).safe

  def run(a: AmazonDynamoDBClient): Task[A] =
    safe.unsafeRun(a)

  def safe: DynamoDBAction[A] =
    DynamoDBAction(
      c =>
        try unsafeRun(c)
        catch { case util.control.NonFatal(t) => Task.fail(t) }
    )
}

object DynamoDBAction {
  def attempt[O](a: Attempt[O]): DynamoDBAction[O] =
    DynamoDBAction { _ =>
      a.fold({
        i => Task.fail(new RuntimeException(i.toString))
      }, { v => Task.now(v) })
    }

  def task[O](t: Task[O]): DynamoDBAction[O] =
    DynamoDBAction { _ => t }

  def withClient[O](f: AmazonDynamoDBClient => O): DynamoDBAction[O] =
    config map f

  def fail[A](msg: String): DynamoDBAction[A] =
    task(Task.fail(new RuntimeException(msg)))

  def value[A](v: => A): DynamoDBAction[A] =
    DynamoDBAction[A](_ => Task.now(v))

  def config: DynamoDBAction[AmazonDynamoDBClient] =
    DynamoDBAction(Task.now)

  def ok[R, A](strict: A): DynamoDBAction[A] =
    value(strict)

  implicit object DynamoDBActionMonad extends Monad[DynamoDBAction] {
    def point[A](v: => A) = DynamoDBAction.ok(v)
    def bind[A, B](m: DynamoDBAction[A])(f: A => DynamoDBAction[B]) = m flatMap f
    override def map[A, B](m: DynamoDBAction[A])(f: A => B) = m map f
  }
}
