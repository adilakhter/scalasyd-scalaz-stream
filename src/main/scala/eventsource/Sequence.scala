package eventsource

case class Sequence(seq: Long)

object Sequence {
  val first = Sequence(0)

  def next(s: Sequence): Sequence =
    Sequence(s.seq + 1)
}