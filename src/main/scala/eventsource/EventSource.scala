package eventsource

import org.joda.time.DateTime
import scalaz.{ Catchable, Monad, \/, \/-, -\/ }
import scalaz.stream.{process1, Process1, Process}
import scalaz.stream.Process.{ Await, End, Halt, Emit }
import com.github.nscala_time.time.Implicits.richReadableInstant

/**
 * An event source is an append-only store of data. Data is represented as a series of events that when replayed in
 * order provides a view of the data at that point in time.
 *
 * In this implementation, an event is represented by a Transform that is contained within a Commit (strictly speaking
 * a Commit could contain a series of Transforms, but we're not doing that here to keep things simple).
 *
 * To implement an event source, one needs to:
 *   - Extend the EventSource trait. An event source provides data of type V for a given key of type K
 *   - Provide an API implementation that creates a suitable Transform for a new value to be saved when given an existing
 *     view of the data.
 *   - Provide Commits implementation that wraps persistence of commits (e.g. DynamoDB, in-memory map, Cassandra).
 *     Persistence stores only need to support the following key-value operations:
 *       - Records are keyed against a hash key (typically a unique identifier of business value) and a numeric range key
 *       - querying for a given hash key
 *       - conditional saves against a given hash and range key to prevent overwriting of a given record.
 */

/**
 * EventSourceError represents any error conditions that are useful to represent for event sources. In particular,
 * we need to know about attempts to store duplicate commits.
 */
sealed trait EventSourceError
object EventSourceError {
  case object DuplicateCommit extends EventSourceError
}

/**
 * The main trait that implementations of an event source need to extend.
 * @tparam K The key against which values are stored.
 * @tparam V Values to be store
 */
trait EventSource[K, V] {

  /**
   * A commit is identified by the key and an incrementing sequence number
   * @param key The key
   * @param sequence the sequence number
   */
  case class CommitId(key: K, sequence: Sequence)

  object CommitId {
    def first(key: K): CommitId =
      CommitId(key, Sequence.first)

    def next(id: CommitId): CommitId =
      CommitId(id.key, Sequence.next(id.sequence))
  }

  case class Commit(id: CommitId, time: DateTime, operation: Transform[V]) {
    def apply(old: Option[V]): Option[V] = operation(old)
  }

  object Commit {
    def next(key: K, snapshot: Snapshot, op: Transform[V]): Commit = {
      import Snapshot._
      val newId = snapshot match {
        case Value(_, at) => CommitId.next(at)
        case NoSnapshot() => CommitId.first(key)
        case Deleted(at)  => CommitId.next(at)
      }
      Commit(newId, DateTime.now, op)
    }
  }

  /**
   * A Snapshot wraps an optional value, and tags it with a commit Id. We can say a 'snapshot' S of key K at commit
   * S.at is value S.value
   *
   * The commit Id is quite a useful thing in addition to the value of the snapshot.
   *
   * This is only used internally within an event source.
   */
  sealed trait Snapshot {
    def value: Option[V]
  }

  object Snapshot {
    /**
     * There is no snapshot... i.e. no commits have been saved.
     */
    case class NoSnapshot() extends Snapshot {
      val value = None
    }

    /**
     * Commits have been saved and there is a value stored.
     * @param view The value
     * @param at
     */
    case class Value(view: V, at: CommitId) extends Snapshot {
      val value = Some(view)
    }

    /**
     * Commits have been saved and there is no value (i.e. the value has been deleted).
     * @param at
     */
    case class Deleted(at: CommitId) extends Snapshot {
      val value = None
    }

    val zero: Snapshot = NoSnapshot()

    def update(s: Snapshot, c: Commit): Snapshot =
      c.operation(s.value) match {
        case None    => Deleted(c.id)
        case Some(v) => Value(v, c.id)
      }
  }

  /**
   * A source of commits. Implementations wrap around an underlying data store (e.g. in-memory map or DynamoDB).
   *
   * @tparam F Container around operations on an underlying data store. F must be a Monad and a Catchable (e.g. Task).
   */
  trait Storage[F[_]] {
    def M: Monad[F]
    def C: Catchable[F]

    /**
     * Retrieve a stream of commits from the underlying data store. This stream should take care of pagination and
     * cleanup of any underlying resources (e.g. closing connections if required).
     * @param key The key
     * @return Stream of commits.
     */
    def get(key: K): Process[F, Commit]

    /**
     * Save the given commit.
     * @param commit The commit to save.
     * @return Either an EventSourceError or the commit that was saved. Other non-specific errors should be available
     *         through the container F.
     */
    def put(commit: Commit): F[EventSourceError \/ Commit]

    /**
     * Essentially a runFoldMap on the given process to produce a snapshot after collapsing a stream of commits.
     * @param commits The stream of commits.
     * @return
     */
    def applyCommits(commits: Process[F, Commit]): F[Snapshot] = {
      def go(cur: Process[F, Commit], acc: Snapshot): F[Snapshot] =
        cur match {
          case Emit(h, t) => go(t, h.foldLeft(acc)((x, y) => Snapshot.update(x, y)))
          case Halt(e) => e match {
            case End => M.point(acc)
            case _   => C.fail(e)
          }
          case Await(req, recv, _, c) =>
            M.bind(C.attempt(req)) {
              case -\/(e)    => go(c.causedBy(e), acc)
              case \/-(page) => go(recv(page), acc)
            }
        }
      go(commits, Snapshot.zero)
    }

    /**
     * Really cool suggestion from etorreborre for implementing applyCommits. Model our functionality as a single-input
     * transducer (which is just a fold), and apply it to the stream of commits.
     */
    val applyCommitsTransducer: Process1[Commit, Snapshot] =
      process1.fold(Snapshot.zero)(Snapshot.update)

    def applyCommitsProcess1(commits: Process[F, Commit]): F[Snapshot] =
      commits.pipe(applyCommitsTransducer).runLastOr(Snapshot.zero)(M, C)
  }

  /**
   * This is the main interface for consumers of the Event source.
   *
   * Implementation will contain logic to create a transform given a value to save.
   *
   * Upon construction of an API, a suitable Commits store needs to be provided.
   *
   * @tparam F Container type for API operations. It needs to be a Monad and a Catchable (e.g. scalaz Task)
   */
  trait API[F[_]] {
    def M: Monad[F]
    def C: Catchable[F]

    /**
     * @return Underlying store of commits
     */
    def store: Storage[F]

    /**
     * Create a suitable transform from 'old' to 'newValue'.
     * @param old The old value
     * @param newValue The new value
     * @return The suitable transform.
     */
    def createTransform(old: Option[V], newValue: Option[V]): Transform[V]

    /**
     * Return the current view of the data for key 'key'
     * @param key the key
     * @return current view of the data
     */
    def get(key: K): F[Option[V]] =
      getAtUsing(store)(key, _ => true)

    /**
     * Return the view of the data for the key 'key' at the specified sequence number.
     * @param key the key
     * @param seq the sequence number of the commit at which we want the see the view of the data.
     * @return view of the data at commit with sequence 'seq'
     */
    def getAt(key: K, seq: Sequence): F[Option[V]] =
      getAtUsing(store)(key, { _.id.sequence.seq <= seq.seq })

    /**
     * Return the view of the data for the key 'key' at the specified timestamp.
     * @param key The key
     * @param time The timestamp at which we want to see the view of the data
     * @return view of the data with commits up to the given time stamp.
     */
    def getAt(key: K, time: DateTime): F[Option[V]] = {
      import com.github.nscala_time.time.Implicits._
      getAtUsing(store)(key, { _.time <= time })
    }

    /**
     * Save the given value for the given key.
     * @param key The key
     * @param value The value to save
     * @return The previous value if there was one.
     */
    def save(key: K, value: Option[V]): F[Option[V]] =
      saveUsing(store)(key, value)

    /**
     * To save a new value, we need to get the latest snapshot in order to get the existing view of data and the
     * latest commit Id. Then we create a suitable transform and commit and try to save it. Upon duplicate commit,
     * try the operation again (highly unlikely that this situation would occur).
     * @param store The Commits store to use.
     * @param key The key
     * @param value The value to store
     * @return The previous view of the data.
     */
    private[eventsource] def saveUsing(store: Storage[F])(key: K, value: Option[V]): F[Option[V]] = {
      import scalaz.syntax.monad._
      implicit def MonadF = M
      for {
        latest <- store.applyCommitsProcess1(store.get(key))
        result <- store.put(Commit.next(key, latest, createTransform(latest.value, value)))
        check <- result match {
          case -\/(EventSourceError.DuplicateCommit) => saveUsing(store)(key, value)
          case \/-(c)                                => latest.value.point[F]
        }
      } yield check
    }

    /**
     * All a 'get' is doing is taking commits up to a condition (e.g. sequence number or a date) and then applying
     * them in order. This is quite trivial using something like Scalaz Stream.
     * @param store The store that provides a stream of commits.
     * @param key The key for which to retrieve commits.
     * @param cond Conditional function for filtering commits.
     * @return View of the data obtained from applying all commits in the stream up until the given condition is not met.
     */
    private[eventsource] def getAtUsing(store: Storage[F])(key: K, cond: Commit => Boolean): F[Option[V]] =
      M.apply(store.applyCommitsProcess1(store.get(key).takeWhile(cond))) { _.value }
  }
}
