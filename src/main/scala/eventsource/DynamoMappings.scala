package eventsource

import eventsource.aws.dynamodb.Encoder._
import eventsource.aws.dynamodb._
import kadai.Attempt
import org.joda.time.DateTime

object DynamoMappings {

  implicit def LogicalKeyEncode: Encoder[LogicalKey] =
    Encoder[String].contramap { k: LogicalKey => k.key }
  implicit def LogicalKeyDecode: Decoder[LogicalKey] =
    Decoder[String].map { LogicalKey(_) }

  implicit def ContentKeyEncode: Encoder[ContentKey] =
    Encoder[String].contramap { k: ContentKey => k.hash }
  implicit def ContentKeyDecode: Decoder[ContentKey] =
    Decoder[String].map { ContentKey(_) }

  implicit def TransformOpEncode: Encoder[TransformOp] =
    Encoder[String].contramap { TransformOp.apply }
  implicit def TransformOpDecode: Decoder[TransformOp] =
    Decoder[String].flatMap { op =>
      TransformOp(op) match {
        case Some(operation) => Decoder.ok(operation)
        case None            => Decoder.from { Attempt.fail(s"Invalid operation: $op") }
      }
    }

  implicit def SequenceEncode: Encoder[Sequence] =
    Encoder[Long].contramap { case Sequence(l) => l }
  implicit def SequenceDecode: Decoder[Sequence] =
    Decoder[Long].map { Sequence.apply }


  object Mappings {
    val logicalKey = Column[LogicalKey]("LogicalKey")
    val sequence = Column[Sequence]("Sequence")
    val contentKey = Column[ContentKey]("ContentKey")
    val operation = Column[TransformOp]("Operation")
    val lastModified = Column[DateTime]("LastModifiedTimestamp")
  }

  import eventsource.DynamoMappings.Mappings._

  implicit val LogicalKeyMarshaller =
    Marshaller.fromColumn[LogicalKey](logicalKey)

  implicit val MappingObjectMarshaller =
    Marshaller.from[ContentKey] { a =>
      Map(
        contentKey(a)
      )
    }

  implicit val MappingObjectUnmarshaller = Unmarshaller.from[ContentKey](contentKey.get)

  implicit val MappingDynamoValue: StoreValue[ContentKey] =
    StoreValue.withUpdated {
      (_, a) => StoreValue.newFromValues(a)
    }
}
