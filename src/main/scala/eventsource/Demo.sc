import com.amazonaws.regions.RegionUtils
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient
import eventsource.MappingEventSource
import eventsource.LogicalKey
import eventsource.ContentKey
import scalaz.syntax.id._

val commits = new MappingEventSource.DynamoCommits(new AmazonDynamoDBClient() <| { _.setRegion(RegionUtils.getRegion("ap-southeast-2")) })
val api = new MappingEventSource.MappingAPI(commits)

// Insert a bunch of commits
val key = LogicalKey("abcd")
api.save(key, Some(ContentKey("def"))).run
api.save(key, Some(ContentKey("ghi"))).run
api.save(key, None).run
api.save(key, Some(ContentKey("xyz"))).run

// Read all of them
val commitStream = commits.get(key)
commits.applyCommits(commitStream).run

// Get at 2
val truncatedCommitStream = commitStream.takeWhile { _.id.sequence.seq <= 2 }
commits.applyCommits(truncatedCommitStream).run
