package eventsource

trait DomainTypes {
  case class LogicalKey(key: String)
  case class ContentKey(hash: String)
}

