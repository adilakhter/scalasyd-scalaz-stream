package copynhash

import scalaz.stream.Process
import scalaz.stream.io
import kadai.hash.SHA1

object Copy extends App {
  case class Config(src: String = "", dest: String = "", bufferSize: Int = 4096)

  val parser = new scopt.OptionParser[Config]("copy") {
    arg[String]("<src>") required() action { (x, c) =>
        c.copy(src = x) }
    arg[String]("<dest>") required() action { (x, c) =>
      c.copy(dest = x) }
    opt[Int]('b', "buffer") action { (x, c) =>
      c.copy(bufferSize = x) }
  }

  parser.parse(args, Config()) map { config =>
    val hash =
      Process.constant(config.bufferSize)
        .through(io.fileChunkR(config.src))             // Read using buffer size
        .through(io.fileChunkW(config.dest).toChannel)  // Write to file (sink), and spit out what was written
        .map { _.toArray }                              // SHA1.Digester works with Array[Byte], not ByteVector
        .runFoldMap { bv => SHA1.Digester(bv.toArray) } // SHA1.Digester is a 'monoid'. This folds over the stream, adding to the Digester
        .map { SHA1.Digester.compute }                  // Compute the digest
        .run

    println(s"File hash: ${hash.toBase16}")
  }
}
