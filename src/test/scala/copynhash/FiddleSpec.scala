package copynhash

import java.io.ByteArrayInputStream

import kadai.hash.SHA1
import org.scalacheck.{Gen, Arbitrary, Prop}
import org.specs2.{ScalaCheck, SpecificationWithJUnit}

import scala.util.Random
import scalaz.stream._
import Process._
import scalaz.{@@, Tag}
import scalaz.syntax.id._

class FiddleSpec extends SpecificationWithJUnit with ScalaCheck {

  def is = s2"""
                Scalaz Stream hash works $works
  """

  def works = Prop.forAll { (b: LargeContent) =>
    val fromKadai = SHA1.hashOf(b)

    val fromScalazStream =
      Process.constant(4096)
        .through(scalaz.stream.io.chunkR(new ByteArrayInputStream(b)))
        .map { _.toArray }
        .runFoldMap(SHA1.Digester)
        .map { d => SHA1.Digester.compute(d).toBase16 }
        .run

    fromScalazStream === fromKadai.toBase16
  }

  sealed trait Large

  type LargeContent = Array[Byte] @@ Large

  implicit def arbitraryLargeContent: Arbitrary[LargeContent] = Arbitrary(
    Gen.chooseNum(1, 1000000).map(size => new Array[Byte](size) <| ((new Random).nextBytes)).map(Tag.apply)
  )
}
