Code and slides for Scalaz Stream talk (ScalaSyd July 2014)
===

Slides are in the `docs` directory.

The main bits of code are:

   - `eventsource` package has the Event Sourcing example. The interesting Scalaz Stream bits are:
       - `EventSource.scala` - specifically `EventSource.Storage.applyCommits` and `EventSource.Storage.applyCommitsProcess1`
       - `MappingEventSource.scala` - specifically the `MappingEventSource.DynamoCommits.get` function.
       - There is a Scala worksheet `Demo.sc` with statements for running the demo (you'll need AWS credentials set up in the usual places, and a Dynamo table created (currently with name `sshek-demo-mapping-commits`, hash key `LogicalKey` (string) and range key `Sequence` (number)).
   - `copynhash` package has the Copy 'n' hash example. This can be run by `sbt "run srcfile destfile"`